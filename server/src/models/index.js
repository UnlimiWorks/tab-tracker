const fs = require('fs')
const path = require('path')
const Sequelize = require('Sequelize')
const config = require('../config/config')

const sequelize = new Sequelize(
  config.db.name,
  config.db.user,
  config.db.password,
  config.db.options
)

const db = {}

fs.readdirSync(__dirname)
  .filter(fileName => fileName !== 'index.js')
  .forEach(fileName => {
    const model = sequelize.import(path.join(__dirname, fileName))
    db[model.name] = model
  })

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
